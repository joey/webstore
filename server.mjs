import path from "path"
import express from "express"
import fs, { read } from "fs"
import MakeOverridingRingBuffer from "./src/MakeOverridingRingBuffer.mjs"
import ExpressJsRenderer from "./src/ExpressJsRenderer.mjs"
import BodyParser from "body-parser"
import MethodOverride from "method-override"
import hbs from "hbs"
import ProductEndpoints from "./products/endpoints.mjs"
import IndexEndpoints from "./index/endpoints.mjs"
import MakeObservableList from "./src/MakeObservableList.mjs"
import MakeKeyValueObservable from "./src/MakeKeyValueObservable.mjs"
import { FILE } from "dns"

const moduleURL = new URL(import.meta.url)
const File = fs.promises
const app = express()
const __dirname = path.dirname(moduleURL.pathname)
const DATA_FOLDER = path.resolve(__dirname, ".data")
const PRODUCTS_FILE_NAME = path.resolve(DATA_FOLDER, "products.json")

express.static.mime.define({"text/javascript": ["js", "mjs"]})
app.use(express.static(path.join(__dirname, "src")))
app.use(BodyParser.urlencoded({ extended: false }))
app.use(BodyParser.json())
app.use(MethodOverride("_method"))

//app.use("/css", express.static("css"))
//app.set("view", View)

app.engine("html", hbs.__express)
app.set("views", __dirname)
app.set("view engine", "html")

hbs.registerPartial("availability/product", fs.readFileSync(path.resolve(__dirname, "availability/product.html"), "utf-8"))

const writeToDisk = async () => {
  await File.writeFile(PRODUCTS_FILE_NAME, JSON.stringify(products))
}

let products = MakeObservableList()
products = ProductEndpoints(app, products)
let model = MakeKeyValueObservable({title: "Home Page"})
model = IndexEndpoints(app, model)

File.stat(DATA_FOLDER)
  .then(() => {
    readProductsFromFile()
  })
  .catch(async () => {
    File.mkdir(DATA_FOLDER)
    await File.writeFile(PRODUCTS_FILE_NAME, "[]", "utf-8")
    readProductsFromFile()
  })

const readProductsFromFile = ()=>{
  const data = fs.readFileSync(PRODUCTS_FILE_NAME, "utf-8")
  if(data) JSON.parse(data).forEach(product => products.push(product))  
}

const PersistProducts = (key, old, value) => {
  console.log(key, value)
  writeToDisk().then().catch(err => console.error(err))
}
products.observe("push", PersistProducts)
products.observe("remove", PersistProducts)

const messages = MakeOverridingRingBuffer({max: 5000})
app.get("/messages", (req, res) => {
  let message = null
  let current = []
  while(message = messages.shift()){
    if(current[current.length-1] === message) break
    current.push(message)
  }
  res.json(current)
})

const listener = app.listen(process.env.PORT, () => {
  console.log(`Service istening on port http://localhost:${listener.address().port}`)
})

const handler = async () => {
  console.log("shutting down")
  await writeToDisk()
  process.exit()
}

;["SIGINT", "SIGTERM"].forEach( sig => {
  process.on(sig, handler)
})

