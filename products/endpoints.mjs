import uuid from "uuid"

class Product {
  constructor(obj){
    this.title = obj.title
    this.description = obj.description
    this.id = obj.id || uuid.v4()
  }
}
const sanitize = obj => {
  return obj
}

const ProductEndpoints = (app, products) => {

  app.get("/products", async (req, res) => {
    res.render("products/index", { layout: "layouts/default", title: "List of products", products: products})
  })

  app.get("/products/add", async (req, res) => {
    res.render("products/add", { layout: "layouts/default", title: "New Product", description: "Some description" })
  })

  app.post("/products/add", async (req, res) => {
    let product = new Product(sanitize(req.body))
    products.push(product)
    res.render("products/add", { layout: "layouts/default", title: "New Product", description: "Some description" })
  })
  app.delete("/products/:id", async (req, res) => {
    const id = req.params.id
    products.remove(p=>p.id == id)
    res.render("products/index", { layout: "layouts/default", title: "List of products", products: products})
  })
  app.get("/products/:id", async (req, res) => {
    const id = req.params.id
    const product = products.find(p=>p.id == id)
    res.render("products/detail", {layout: "layouts/default", title: product.Title, product: product})
  })
  return products
}
export default ProductEndpoints