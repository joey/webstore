import tap from "tap"
import MakeObservableList from "../src/MakeObservableList.mjs"
import MakeOverridingRingBuffer from "../src/MakeOverridingRingBuffer.mjs"
tap.test("Writer is ahead of the reader", async t => {
  let messages = MakeOverridingRingBuffer({max: 5})
  messages.push(1)
  messages.push(2)
  let actual = messages.shift()
  t.ok(actual == 1)
  t.end()
})
tap.test("Writer and Reader are at the same location, haven't looped through the ring yet", async t => {
  let messages = MakeOverridingRingBuffer({max: 3})
  messages.push(1)
  messages.push(2)
  messages.shift()
  let actual = messages.shift()
  t.ok(actual == 2)
  t.end()
})

tap.test("Reader is passed writer, haven't looped through the ring yet", async t => {
  let messages = MakeOverridingRingBuffer({max: 3})
  messages.push(1)
  messages.shift()
  messages.shift()
  let actual = messages.shift()
  t.ok(actual == undefined)
  t.end()
})

tap.test("Writer is at the end, reader reads up to it", async t => {
  let messages = MakeOverridingRingBuffer({max: 3})
  messages.push(1)
  messages.push(2)
  messages.push(3)
  let actual = messages.shift()
  t.ok(actual == 1)
  t.ok(messages.shift() == 2)
  t.ok(messages.shift() == 3)
  t.ok(messages.shift() == 1)
  let actual2 = messages.shift()
  t.ok(actual == 1)
  t.end()
})

tap.test("Let's do this 1000s of times", async t => {
  let messages = MakeOverridingRingBuffer({max: 10})
  let i = 0
  for(i; i < 103; i++){
    messages.push(`${i} message`)
  }
  for(i = 0; i < 10; i++){
    messages.shift()
  }
  t.ok(messages.shift() == `102 message`)
  t.end()
})

tap.test("Let's do this 1000s of times, then push and read another message", async t => {
  let messages = MakeOverridingRingBuffer({max: 10})
  let i = 0
  for(i; i < 103; i++){
    messages.push(`${i} message`)
  }
  for(i = 0; i < 10; i++){
    messages.shift()
  }
  console.log("shifting", messages.shift())
  let expected = `ok new message`
  messages.push(expected)
  t.ok(messages.shift() == expected)
  t.end()
})


