import vm from "vm"
import tap from "tap"
import fs from "fs"
import jsdom from "jsdom"
import path from "path"
const {JSDOM, VirtualConsole} = jsdom

const moduleURL = new URL(import.meta.url)
const __dirname = path.dirname(moduleURL.pathname)
const File = fs.promises
const makeDom = data => {
    let c = new VirtualConsole()
    c.sendTo(console)
    return new jsdom.JSDOM(data, {
        url: "https://example.org/",
        referrer: "https://example.com/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 10000000,
        pretendToBeVisual: true
    })
}

const domFromFile = async file => {
  let data = null
  try {
      data = await File.readFile(path.join(__dirname, file), {encoding: "utf-8"})
  } catch(e) {
      console.error(e)
  }
  return makeDom(data)
}

const fromFile = async file => {
  let data = null
  try {
      data = await File.readFile(path.resolve(__dirname, file), {encoding: "utf-8"})
  } catch(e) {
      console.error(e)
  }
  return data
}

tap.test("Loading a partial html file", async t => {
  let dom = await JSDOM.fromFile(path.resolve(__dirname, "templating/index.phtml"))
  const field = dom.window.document.getElementById("field")
  t.ok(field.outerHTML == `<div id="field"></div>`)
  dom.window.close()
  t.end()
})

tap.test("Loading a layout HTML file", async t => {
  let dom = await JSDOM.fromFile(path.resolve(__dirname, "templating/layout.html"))
  let availabilityHtml = await fromFile("templating/availability/current.phtml")
  const html = dom.window.document.querySelector(`[role="main"`)
  html.innerHTML = availabilityHtml
  t.ok(dom.window.document.getElementById("availability").textContent == "In Stock.")
  dom.window.close()
  t.end()
})

const TemplateWithLayout = async (layout, partial, model) => {
  let dom = await JSDOM.fromFile(path.resolve(__dirname, layout))
  let phtml = await fromFile(partial)
  return {
    toString(){
      const html = dom.window.document.querySelector(`[role="main"`)
      html.innerHTML = phtml
      Object.keys(model).forEach( key => {
        dom.window.document.getElementById(key).textContent = model[key]
      })
      return html.outerHTML
    }
  }
}

tap.test("Using the API, load a template", async t => {
  const html = await TemplateWithLayout("templating/layout.html", "templating/availability/current.phtml", {availability: "Unavailable"})
  t.ok(html.toString().indexOf("Unavailable") > -1)
  t.end()
})