import tap from "tap"
import MakeKeyValueObservable from "../src/MakeKeyValueObservable.mjs"
import MakeObservableList from "../src/MakeObservableList.mjs"


const Message = obj => {
  return Object.assign({}, obj)
}

tap.test("Receive a new message", t => {
  let messages = MakeObservableList()
  messages.observe("push", (key, old, value)=>{
    t.ok(value.text == "hi")
    t.end()
  })
  messages.push(Message({text: "hi"}))
})

tap.test("What's the last message on the list?", t => {
  let messages = MakeObservableList()
  const expected = "hi, 2nd message"
  messages.observe("pop", (key, old, value)=>{
    t.ok(old.text == expected)
  })
  messages.push(Message({text: "hi, 1st message"}))
  messages.push(Message({text: expected}))
  let m = messages.pop()
  t.ok(m.text == expected)
  t.end()
})

tap.test("What's the first item?", t => {
  let messages = MakeObservableList()
  const expected = "hi, 1st message"
  messages.observe("shift", (key, old, value)=>{
    t.ok(value.text == expected)
  })
  messages.push(Message({text: expected}))
  messages.push(Message({text: "hi, 2nd message"}))
  console.log(messages)
  let first = messages.shift()
  t.ok(first.text == expected)
  t.end()
})

tap.test("Signature is ...args", t => {
  let messages = MakeObservableList({text: "first message"}, {text: "second message"})
  const expected = "first message"
  let first = messages.shift()
  t.ok(first.text == expected)
  let second = messages.shift()
  t.ok(second.text = "second message")
  t.end()
})

