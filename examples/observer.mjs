import tap from "tap"
import MakeKeyValueObservable from "../src/MakeKeyValueObservable.mjs"
tap.test("Should observe a change", t => {
  let model = MakeKeyValueObservable({firstName: "joey", lastName: "guerra"})
  const observer = {
    update(old, key, value){
      model.name = `${model.firstName} ${model.lastName}`
    }
  }
  model.observe("firstName", observer)
  model.observe("lastName", observer)
  model.firstName = "notjoey"
  model.lastName = "notguerra"
  t.ok(model.name === `${model.firstName} ${model.lastName}`)
  t.end()
})

tap.test("an observer can be a function", t => {
  let model = MakeKeyValueObservable({firstName: "joey", lastName: "guerra", age: 48})
  model.observe("firstName", (key, old, v) => {
    t.ok(v === "notjoey")
    t.end()
  })
  model.firstName = "notjoey"
})

tap.test("should stop observing", t => {
  let model = MakeKeyValueObservable({firstName: "joey", lastName: "guerra", age: 48})
  const observer = {
    update(old, key, value){
      model.name = `${model.firstName} ${model.lastName}`
    }
  }
  
  model.observe("firstName", observer)
  model.firstName = "notjoey"
  t.ok(model.firstName === "notjoey")
  
  t.end()
})