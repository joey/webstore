import tap from "tap"
import express from "express"
import path from "path"
import ExpressJsRenderer from "../src/ExpressJsRenderer.mjs"

const moduleURL = new URL(import.meta.url)
const __dirname = path.dirname(moduleURL.pathname)

tap.test("interpolates a provided string", async t => {
  const titleTpl = "${engineName} - The fastest javascript template string engine!";
  const content = await ExpressJsRenderer(titleTpl, {
    template: true,
    locals: { engineName: "Template Literal Engine" }
  })
  t.ok(content == "Template Literal Engine - The fastest javascript template string engine!")
  t.end()
})

tap.test("throws an error in case of interpolation failure", async t => {
  const titleTpl = "${engineName} - The fastest javascript template string engine!";
  try{
    await ExpressJsRenderer(titleTpl, {
      template: true,
      locals: {}
    })
  } catch(err){
    t.ok(err instanceof Error)    
  } finally {
    t.end()
  }
})

tap.test("renders a template file with a callback", async t => {
  await ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    { locals: { engineName: "Template Literal Engine", footer: "MIT License" } },
    (err, content) => {
      t.ok(err == null)
      t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
      t.end()
    }
  )
})

tap.test("throws an error in case of template interpolation failure with a callback", async t => {
  try{
    await ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
      { locals: { footer: "MIT License" } },
      err => {
        t.ok(err instanceof Error)
      }
    )
  } catch(err){
    t.ok(err instanceof Error)
  } finally {
    t.end()
  }
})

tap.test("renders a template file with a promise", t => {
  ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    { locals: { engineName: "Template Literal Engine", footer: "MIT License" } }
  ).then(content => {
    t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
    t.end()
  })
})

tap.test("renders a template file with both promise and callback", t => {
  ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    { locals: { engineName: "Template Literal Engine", footer: "MIT License" } },
    (err, content) => {
      t.ok(err == null)
      t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
    }
  ).then((content) => {
    t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
    t.end()
  })
})

tap.test("throws an error in case of template interpolation with promise failure", t => {
  const willRender = ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    { locals: {} }
  )
  willRender.catch((err) => {
    t.ok(err instanceof Error)
    t.end()
  })
})

tap.test("throws an error in case of template interpolation with both promise and callback", t => {
  ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    { locals: { footer: "MIT License" } },
    (err, content) => {
      t.ok(err instanceof Error)
    }
  ).catch(err => {
    t.ok(err instanceof Error)
    t.end()
  })
});

tap.test("merges a string and a partial file with both promise and callback", t => {
  const template = "${engineName} - The fastest javascript template string engine!${footer}"
  const willRender = ExpressJsRenderer(
    template,
    {
      template: true,
      locals: { engineName: "Template Literal Engine", footer: "MIT License" },
      partials: { footer: path.resolve(__dirname, "templating/partial.html")}
    },
    (err, content) => {
      t.ok(err == null)
      t.ok(content == "Template Literal Engine - The fastest javascript template string engine!MIT License")
    }
  );
  willRender.then((content) => {
    t.ok(content == "Template Literal Engine - The fastest javascript template string engine!MIT License")
    t.end()
  })
})

tap.test("render partials", async t => {
  await ExpressJsRenderer(path.resolve(__dirname, "templating/es6index.html"),
    {
      locals: { engineName: "Template Literal Engine" },
      partials: {
        footer: path.resolve(__dirname, "templating/partial.html")
      }
    },
    (err, content) => {
      t.ok(err == null)
      t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
      t.end()
    }
  )
})

tap.test("throws an error when template is not found", t => {
  ExpressJsRenderer(
    __dirname + "/notfound.html",
    {
      locals: { engineName: "Template Literal Engine" },
      partials: {
        footer: __dirname + "/partial.html"
      }
    },
    err => t.ok(err instanceof Error)
  ).catch((err) => {
    t.ok(err instanceof Error)
    t.end()
  })
})

tap.test("throws an error when partials is not found", t => {
  ExpressJsRenderer(
    __dirname + "/index.html",
    {
      locals: { engineName: "Template Literal Engine" },
      partials: {
        footer: __dirname + "/partia.html"
      }
    },
    err => t.ok(err instanceof Error)
  ).catch((err)=>{
    t.ok(err instanceof Error)
    t.end()
  });
})

tap.test("can pre-compile templates when all names are listed", async t => {
  const text = '${engineName} - The fastest javascript template string engine in the whole ${place}!'
  const precompiled = await ExpressJsRenderer(text, 'engineName, place')
  const content = precompiled("Template Literal Engine", "multiverse")
  t.ok(typeof precompiled == "function")
  t.ok(content == "Template Literal Engine - The fastest javascript template string engine in the whole multiverse!")
  t.end()
})

tap.test("can precompile templates using default '$' object property", async t => {
  const text = '${$.engineName} - The fastest javascript template string engine in the whole ${$.place}!'
  const precompiled = await ExpressJsRenderer(text)
  const content = precompiled({ engineName: 'Template Literal Engine', place: "multiverse" })
  t.ok(typeof precompiled == "function")
  t.ok(content == "Template Literal Engine - The fastest javascript template string engine in the whole multiverse!")
  t.end()
})

tap.test("throws an error on template precompilation failure", async t => {
  const text = '${engineName} - The fastest javascript template string engine in the whole ${place}!'
  const precompiled = await ExpressJsRenderer(text, 'engineName')
  const err = precompiled("Template Literal Engine", "multiverse")
  t.ok(typeof precompiled == "function")
  t.ok(err instanceof Error)
  t.end()
})

tap.test("Express", finalTest => {
  const app = express()

  app.engine("html", ExpressJsRenderer)
  app.set("views", path.resolve(__dirname, "templating"))
  app.set("view engine", "html")

  finalTest.test("renders a template file", t => {
    app.render(
      "es6index",
      { locals: { engineName: "Template Literal Engine", footer: "MIT License" } },
      (err, content) => {
        t.ok(err == null)
        t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
        t.end()
      }
    )
  })

  finalTest.test("render partials", t => {
    app.render(
      "es6index",
      {
        locals: { engineName: "Template Literal Engine" },
        partials: {
          footer: "partial"
        }
      },
      (err, content) => {
        t.ok(err == null)
        t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
        t.end()
      }
    )
  })
  
  finalTest.test("render with just an object", t => {
    app.render("es6index", {engineName: "Template Literal Engine", partials: {footer: "partial"}}, (err, content)=>{
      t.ok(err == null)
      t.ok(content == "Template Literal Engine - The fastest javascript template string engine!\nMIT License")
      t.end()
    })
  })
  finalTest.end()
})