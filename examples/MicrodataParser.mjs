import tap from "tap"
import jsdom from "jsdom"
import MicrodataParser from "../src/MicrodataParser.mjs"

const {JSDOM, VirtualConsole} = jsdom
const makeDom = data => {
    let c = new VirtualConsole()
    c.sendTo(console)
    return new JSDOM(data, {
        url: "https://example.org/",
        referrer: "https://example.com/",
        contentType: "text/html",
        includeNodeLocations: true,
        storageQuota: 10000000,
        pretendToBeVisual: true
    })
}

tap.test("Parsing a couple of items", t => {
    const peopleHtml = `
<div itemscope>
    <p>My name is <span itemprop="name">Elizabeth</span>.</p>
</div>

<div itemscope>
    <p>My name is <span itemprop="name">Daniel</span>.</p>
</div>
`
    const dom = makeDom(peopleHtml)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items.length, 2, "Should have 2 items")
    t.end()
})
tap.test("Parsing a couple of items with mixed HTML", t => {
    const mixedHtmlElements = `
<div itemscope>
 <p>My <em>name</em> is <span itemprop="name">E<strong>liz</strong>abeth</span>.</p>
</div>

<section>
 <div itemscope>
  <aside>
   <p>My name is <span itemprop="name"><a href="/?user=daniel">Daniel</a></span>.</p>
  </aside>
 </div>
</section>
`

    const dom = makeDom(mixedHtmlElements)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items.length, 2, "Should have 2 items")
    t.end()
})

tap.test("Has three properties", t => {
    const html = `    
<div itemscope>
    <p>My name is <span itemprop="name">Neil</span>.</p>
    <p>My band is called <span itemprop="band">Four Parts Water</span>.</p>
    <p>I am <span itemprop="nationality">British</span>.</p>
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties.length, 3, "Should have 3 items")
    t.end()

})

tap.test("Img value is a URL", t => {
    const html = `
<div itemscope>
    <img itemprop="image" src="google-logo.png" alt="Google">
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].value, "google-logo.png", "Should be the value of the src attribute")
    t.end()
})

tap.test("Data tag value from the valute attribute", t => {
    const html = `
<h1 itemscope>
    <data itemprop="product-id" value="9678AOU879">The Instigator 2000</data>
</h1>
`    
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].value, "9678AOU879", "Should be the value")
    t.end()
})

tap.test("Meter tag should get value from value attribute", t => {
    const html = `
<div itemscope itemtype="http://schema.org/Product">
    <span itemprop="name">Panasonic White 60L Refrigerator</span>
    <img src="panasonic-fridge-60l-white.jpg" alt="">
    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <meter itemprop="ratingValue" min=0 value=3.5 max=5>Rated 3.5/5</meter>
        (based on <span itemprop="reviewCount">11</span> customer reviews)
    </div>
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[1].value.properties[0].value, "3.5", "Should be the value")
    t.end()
})

tap.test("Time element", t => {
    const html = `
<div itemscope>
I was born on <time itemprop="birthday" datetime="2009-05-10">May 10th 2009</time>.
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].value, "2009-05-10", "Should be from datetime attribute")
    t.end()
})

tap.test("Nested properties", t => {
    const html = `
<div itemscope>
    <p>Name: <span itemprop="name">Amanda</span></p>
    <p>Band: <span itemprop="band" itemscope> <span itemprop="name">Jazz Band</span> (<span itemprop="size">12</span> players)</span></p>
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[1].value.properties[0].value, "Jazz Band", "Nested value should have a name property")
    t.equals(items[0].properties[1].value.properties[1].value, "12", "Other property should be 12")
    t.end()
})

tap.test("Nested properties", t => {
    const html = `
<div itemscope>
    <p>Flavors in my favorite ice cream:</p>
    <ul>
        <li itemprop="flavor">Lemon sorbet</li>
        <li itemprop="flavor">Apricot sorbet</li>
    </ul>
</div>

`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].value, "Lemon sorbet", "Properties with the same key and different values")
    t.equals(items[0].properties[1].value, "Apricot sorbet", "Properties with the same key and different values")
    t.end()
})


tap.test("Set item properties with the same value", t => {
    const html = `
<div itemscope>
    <span itemprop="favorite-color favorite-fruit">orange</span>
</div>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].key, "favorite-color", "Can set multiple properties with the same value")
    t.equals(items[0].properties[1].key, "favorite-fruit", "Can set multiple properties with the same value")
    t.end()
})


tap.test("Item type", t => {
    const html = `
<section itemscope itemtype="https://example.org/animals#cat">
    <h1 itemprop="name">Hedral</h1>
    <p itemprop="desc">Hedral is a male american domestic
    shorthair, with a fluffy black fur with white paws and belly.</p>
    <img itemprop="img" src="hedral.jpeg" alt="" title="Hedral, age 18 months">
</section>

`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    const obj = parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].types[0], "https://example.org/animals#cat", "Type is set")
    t.equals(items[0].properties.length, 3, "Has 3 properties")
    t.end()
})

tap.test("Global identifier", t => {
    const html = `
<dl itemscope
    itemtype="https://vocab.example.net/book"
    itemid="urn:isbn:0-330-34032-8">
 <dt>Title
 <dd itemprop="title">The Reality Dysfunction
 <dt>Author
 <dd itemprop="author">Peter F. Hamilton
 <dt>Publication date
 <dd><time itemprop="pubdate" datetime="1996-01-26">26 January 1996</time>
</dl>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    const obj = parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].id, "urn:isbn:0-330-34032-8", "Global id is set")
    t.end()
})
tap.test("Global identifier", t => {
    const html = `
<section itemscope itemtype="https://example.org/animals#cat">
    <h1 itemprop="name https://example.com/fn">Hedral</h1>
    <p itemprop="desc">Hedral is a male american domestic shorthair, with a fluffy <span itemprop="https://example.com/color">black</span> fur with <span itemprop="https://example.com/color">white</span> paws and belly.</p>
    <img itemprop="img" src="hedral.jpeg" alt="" title="Hedral, age 18 months">
</section>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[0].value, "Hedral")
    t.equals(items[0].properties[1].value, "Hedral")
    t.equals(items[0].properties[1].key, "https://example.com/fn")
    t.equals(items[0].properties[2].value, `Hedral is a male american domestic shorthair, with a fluffy <span itemprop="https://example.com/color">black</span> fur with <span itemprop="https://example.com/color">white</span> paws and belly.`)
    t.equals(items[0].properties[3].value, "black")
    t.equals(items[0].properties[3].key, "https://example.com/color")
    t.equals(items[0].properties[4].value, "white")
    t.equals(items[0].properties[4].key, "https://example.com/color")
    t.equals(items[0].properties[5].value, "hedral.jpeg")
    t.end()
})

tap.test("Parsing a Product", t=> {
    const html = `
<ul>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
        <img alt="Photo of {{title}}" itemprop="image" src="{{imageUrl}}">
        <a itemprop="url" href="/products/{{id}}" title="{{title}}">
            <span itemprop="title">{{title}}</span>
        </a>
        <form action="/products/{{id}}?_method=DELETE" method="post">
            <input type="hidden" name="_method" value="DELETE"/>
            <button type="submit">del</button>            
        </form>
    </li>
</ul>
`
    const dom = makeDom(html)
    const parser = MicrodataParser()
    const items = []
    parser.execute(dom.window.document.body, scope => {
        items.push(scope)
    })
    t.equals(items[0].properties[2].value, `{{title}}`)
    t.equals(items[0].key, "itemListElement")
    t.end()
})
