import repl from "repl"
import dgram from "dgram"
import os from "os"
const port = process.argv.pop()
const networkInterfaces = os.networkInterfaces()
const keys = Object.keys(networkInterfaces)
const ipv4s = []
keys.forEach(key=>{
    const n = networkInterfaces[key].find(i =>i.family == "IPv4")
    if(n) ipv4s.push(n)
})
let PORT = 20000
let MULTICAST_ADDR = "224.0.0.114"
const socket = dgram.createSocket({ type: "udp4", reuseAddr: true })
const send = message => {
    socket.send(message, 0, message.length, PORT, MULTICAST_ADDR, (e, index)=>{
        //console.log(`Sent message "${message}" ${e} ${index}`)
    })
}
const myEval = (cmd, context, filename, callback) => {
  const message = Buffer.from(`Message from process ${process.pid}`)
  send(cmd)
  callback(null, "")
}
repl.start({ prompt: '> ', eval: myEval })

const messageWasSent = (message, rinfo) => {
    console.log(`Message from: ${rinfo.address}:${rinfo.port} - ${message}`)
}
const joinMultiCastGroup = multiCastAddress => {
  return ()=>{
      ipv4s.forEach(ip=>{
          socket.addMembership(multiCastAddress, ip.address)
          console.log(`Joining multicast on ${ip.address} interface.`)
      })
      const address = socket.address()
      PORT = address.port
      console.log(`UDP socket listening on ${address.address}:${address.port} pid: ${process.pid}`)    
  }
}
//socket.on("message", messageWasSent)
socket.on("listening", ()=>{socket.setBroadcast(true)})
socket.on("listening", joinMultiCastGroup(MULTICAST_ADDR))
socket.bind(58369)