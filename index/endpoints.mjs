const IndexEndpoints = (app, model) => {
  app.get("/", async (req, res) => {
    res.render("index/index", { layout: "layouts/default", model: model})
  })
  return model
}
export default IndexEndpoints