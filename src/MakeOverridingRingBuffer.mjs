class RingBuffer extends Array {
  constructor(...args){
    super(...args)
    this.readPosition = 0
    this.writePosition = 0
    this.count = 0
  }
  push(item){
    this.count++
    this[this.writePosition] = item
    if(this.writerPosition == 1 && this.readerPosition == 0 && this.count > 100) this.count = 0
    this.writePosition = (this.writePosition + 1) % this.length
  }
  shift(){
    if(this.writerPosition == 1 && this.readerPosition == 0 && this.count > 100) this.count = 0
    this.count--
    if(this.count < 0) {
      this.readPosition = this.writePosition
      this.count = 0
      return this[this.readPosition]
    }
    if(this.writePosition > 0 && this.readPosition >= this.writePosition){
      this.readPosition = this.writePosition - 1
      return this[this.readPosition]
    }
    let pos = this.readPosition
    this.readPosition = (this.readPosition + 1) % this.length
    return this[pos]
  }
}

const MakeOverridingRingBuffer = ({max})=>{
  const list = new RingBuffer(max)
  return list
}

export default MakeOverridingRingBuffer