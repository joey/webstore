
import MakeObservable from "./MakeKeyValueObservable.mjs"
const views = []
const subscriptions = {}
const App = {
  init(window){
  },
  publish(key, info){
    subscriptions[key].forEach( s => {
      s[key](info)
    })
  },
  subscribe(key, s){
    subscriptions[key].push(s)
  },
  start(){
  }
}

export default App