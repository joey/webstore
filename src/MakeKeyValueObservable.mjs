const addGetterAndSetter = (target, key, cached, changed) => {
  Reflect.defineProperty(target, key, {
    get(){
      return cached[key]
    },
    set(v){
      let old = cached[key]
      cached[key] = v
      changed(key, old, v)
    },
    enumerable: true
  })
}

const makeApi = (observers) => {
  return {
    observe(key, observer){
      if(!observers[key]) observers[key] = []
      observers[key].push({key, observer})
    },
    stopObserving(observer){
      Object.keys(observers).forEach( key => {
        if(observers[key]) observers[key] = observers[key].filter(o=>o.observer !== observer)
      })
    }
  }
}

const MakeKeyValueObservable = obj => {
  let observers = {}
  let target = {}
  const api = makeApi(observers)
  const changed = (key, old, v) => {
    if(observers[key]) observers[key].forEach( o => o.observer.update ? o.observer.update(key, old, v) : o.observer(key, old, v))
  }
  let cached = Object.assign({}, obj)
  const keys = Object.keys(cached)
  for(let i = 0; i < keys.length; i++){
    let key = keys[i]
    addGetterAndSetter(target, key, cached, changed)
  }
  target = Object.assign(target, api)
  return target
}

export default MakeKeyValueObservable