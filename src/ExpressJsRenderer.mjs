import path from "path"
import fs from "fs"
import TemplateLiteral from "./TemplateLiteral.mjs"
const File = fs.promises

const ExpressJsRenderer = async (filePathOrTemplate, options, delegate) => {
  const functions = [
    function partial(fileName){
      return TemplateLiteral.interpolate(fs.readFileSync(fileName, "utf-8"), ...scoped.keys)(...scoped.values)
    }
  ]
  
  if(options === undefined || typeof options == "string") {
    return TemplateLiteral.compile(filePathOrTemplate, options)
  }
  let scoped = {
    keys: [],
    values: [],
    functions: functions
  }
  if(options.locals){
    scoped.keys = Object.keys(options.locals)
    scoped.values = Object.values(options.locals)
  }
  
  const partialsKeys = options.partials ? Object.keys(options.partials) : []
  const partialsValues = options.partials ? Object.values(options.partials) : []
  
  let viewPath = null
  let fileExtension = null
  if(options.settings){
    viewPath = options.settings.views || null
    fileExtension = options.settings["view engine"] || null
  }
  
  for(let i = 0; i < partialsKeys.length; i++){
    let partialFileName = path.resolve(viewPath, partialsValues[i].indexOf(".") > -1 ? partialsValues[i] : `${partialsValues[i]}.${fileExtension}`)
    let content = await File.readFile(partialFileName, "utf-8")
    scoped.keys.push(partialsKeys[i])
    scoped.values.push(content)
  }
  
  let output = null  
  let template = filePathOrTemplate || null
  if(!options.template){
    template = await File.readFile(filePathOrTemplate, "utf-8")
  }
  let mergedOptions = Object.assign(options, {settings: null, _locals: null})
  delete mergedOptions.settings
  delete mergedOptions.cache
  delete mergedOptions._locals
  delete mergedOptions.locals
  console.log(mergedOptions)
  Object.keys(mergedOptions).forEach( key => {
    scoped.keys.push(key)
    scoped.values.push(mergedOptions[key])
  })
  functions.forEach( fn => {
    scoped.keys.push(fn.name)
    scoped.values.push(fn)
  })
  try {
    console.log(scoped.keys, scoped.values)
    output = TemplateLiteral.interpolate(template, ...scoped.keys)(...scoped.values)
    if(delegate) delegate(null, output)
  } catch(err) {
    if(delegate) delegate(err, null)
    throw err
  }
  return output
}

export default ExpressJsRenderer